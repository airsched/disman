#!/usr/bin/python3
from setuptools import setup

setup(
    name="disman",
    version="0.0a0",
    description="Disruption manager",
    author="Gabriel Hondet",
    packages=['disman', 'test'],
    install_requires=[
        "numpy",
        "scipy>=0.19",
        "pandas>=0.21",
        "fastparquet",
        "pysistence",
    ])
