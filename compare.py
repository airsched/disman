#!/usr/bin/python3
import argparse

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

import disman.agent as Agent
import disman.env as Env
import disman.info as Info


def main(args):
    sns.set()
    simparam = {"p_severe_delay": args.severe_prob, "delays": args.delays}
    idlerew = []
    agentrew = []
    initstate = Env.load(args.schedule, simparam)
    agent = Agent.load(initstate, args.agent)
    idleinfo = []
    agentinfo = []

    for i in range(args.episodes):
        reinit = Env.reset_delays(initstate, args.delays)
        irew, iinf = Agent.idle(reinit)
        arew, ainf = Agent.rollout(agent, reinit)
        idlerew.append(irew)
        agentrew.append(arew)
        idleinfo.append(iinf)
        agentinfo.append(ainf)

    idleform = Info.process(idleinfo)
    agentform = Info.process(agentinfo)
    Info.save(args.out + "_idle", idleform)
    Info.save(args.out + "_agent", agentform)
    df = pd.DataFrame({"idle": idlerew, "agent": agentrew})
    df.to_csv(args.out + ".csv")
    sns.boxenplot(data=df)
    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Compare an agent with idle")
    parser.add_argument(
        "--schedule", type=str, required=True, help="Schedule file")
    parser.add_argument(
        "--agent",
        type=str,
        dest="agent",
        required=True,
        help="Path to agent matrix.")
    parser.add_argument(
        "--iterate",
        type=int,
        dest="episodes",
        required=True,
        help="Number of iterations")
    parser.add_argument(
        "--out", type=str, dest="out", required=False, help="Output file")
    parser.add_argument(
        "--delays",
        type=int,
        dest="delays",
        default=0,
        help="Number of delays per simulation")
    parser.add_argument(
        "--severe_prob",
        type=float,
        dest="severe_prob",
        default=0.,
        help="Probability of severe delay")
    gargs = parser.parse_args()
    main(gargs)
