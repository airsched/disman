import unittest
import random

import _context
import disman.env as Env
import disman.simulator as Simulator


class EnvTest(unittest.TestCase):

    def test_action_conversion(self):
        action = 1
        ia = Env.int_of_action(action)
        rea = Env.action_of_int(ia)
        self.assertEqual(action, rea)

    # def test_state_to_int(self):
    #     """Test if two different states have effectively two different int
    #     repr.
    #     """
    #     sched = Simulator.load(
    #         _context.schedfile
    #     )
    #     n = 100
    #     fleet = tuple(sorted(Simulator.fleet(sched)))
    #     landed = [random.choice(fleet) for _ in range(n)]
    #     delays = [{a: random.randint(0, 1) for a in fleet} for _ in range(n)]
    #     assignment = [{a: random.randint(0, len(fleet) - 1) for a in fleet}
    #                   for _ in range(n)]
    #     remainings = [{a: random.randint(0, 50) for a in fleet}
    #                   for _ in range(n)]
    #     states = [Env.State(schedule=sched, fleet=fleet, landed=l,
    #                         assignment=a, delays=d, remaining=r)
    #               for l, a, d, r
    #               in zip(landed, delays, assignment, remainings)]
    #     converted = [Env.int_of_state(s) for s in states]
    #     for s, si in zip(states, converted):
    #         for t, ti in zip(states, converted):
    #             if s != t:
    #                 self.assertNotEqual(si, ti)
    #             else:
    #                 self.assertEqual(si, ti)

    def test_get_action_set(self):
        initstate = Env.load(
            _context.schedfile
        )
        res = Env.get_action_set(initstate)
        index_of_landed = initstate.fleet.index("a")
        index_of_b = initstate.fleet.index("b")
        self.assertEqual(res, set([index_of_b, index_of_landed]))

    def test_step(self):
        initstate = Env.load(_context.schedfile)
        action = random.choice(list(Env.get_action_set(initstate)))
        newstate = Env.step(initstate, action)
        self.assertNotEqual(newstate, initstate)
