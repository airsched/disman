"""Sets misc vars for tests"""
import os
import sys
import tempfile

sys.path.insert(
    0,
    os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
)

tmpdir = tempfile.mkdtemp()
"""A temporary directory"""
_schedule = """
ac_type,tail_number,origin_airport,destination_airport,scheduled_departure,actual_fp_time
a320,a,LFBO,LFPO,420,90
a320,a,LFPO,LFBO,600,90
a320,a,LFBO,LFML,800,45
a320,b,LFML,LFPO,440,110
a320,b,LFPO,LFLL,660,80
a320,b,LFLL,LFML,850,130
b738,c,LFLL,LFBO,560,100
"""
schedfile = os.path.join(tmpdir, "sched.csv")
with open(schedfile, "w") as f:
    f.write(_schedule)

_sched_withvoid = """
ac_type,tail_number,origin_airport,destination_airport,scheduled_departure,actual_fp_time
a320,a,LFBO,LFPO,500,90
a320,b,LFLL,LFPO,800,120
a320,b,LFPO,LFLL,1000,110
"""
schedvoidfile = os.path.join(tmpdir, "schedvoid.csv")
with open(schedvoidfile, "w") as f:
    f.write(_sched_withvoid)
