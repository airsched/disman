import unittest
from functools import reduce
from numpy import random as spr

import _context
from disman import agent as Agent
from disman import env as Env


class AgentTest(unittest.TestCase):

    def test_make(self):
        initstate = Env.load(_context.schedfile)
        agent = Agent.make(initstate)
        self.assertGreaterEqual(agent.matrix.shape[0],
                                Env.state_max_repr(initstate))
        self.assertGreaterEqual(agent.matrix.shape[1],
                                Env.action_max_repr(initstate))

    def test_update(self):
        pass

    def test_greedy(self):
        initstate = Env.load(_context.schedfile)
        agent = Agent.make(initstate)
        act_set = set(range(Env.action_max_repr(initstate)))
        rstate = spr.randint(Env.state_max_repr(initstate))
        for k in range(Env.action_max_repr(initstate)):
            agent = Agent._set_quality(agent, rstate, k, spr.random())
        # Artificially increase the value of a cell
        raction = spr.choice(list(act_set))
        agent = Agent._set_quality(agent, rstate, raction, 10)
        eps = 0.2
        state_int = Env.state_max_repr(initstate)

        def loop(k):
            if k == 0:
                return []
            else:
                return ([Agent._greedy_policy(agent, rstate, act_set, eps)] +
                        loop(k - 1))
        n = 100
        actions = loop(n)
        nselected = reduce(
            lambda acc, elt: acc + 1 if elt == raction else acc,
            actions, 0)
        self.assertAlmostEqual(nselected,
                               (1 - eps) * n + eps * n / state_int,
                               delta=0.2 * n)
