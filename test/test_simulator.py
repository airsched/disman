"""Tests the simulator

Note that flights are often filtered on the departure time
because it is the easiest way to discriminate them.  There is
no algorithmic-related reason.
"""
import unittest
import _context
import random
from disman import simulator as Simulator


class SimulatorTest(unittest.TestCase):

    def test_load(self):
        sched = Simulator.load(
            _context.schedfile
        )
        self.assertEqual(len(sched.flights), len(sched.delays))

        for h in sched.delays.keys():
            if h.origin != "void":
                # The void flight isn't programmed!
                self.assertIn(h, sched.flights)
        for h in sched.assignment.keys():
            if h.origin != "void":
                # The void flight isn't programmed!
                self.assertIn(h, sched.flights)

        self.assertEqual(set(sched.fleet.keys()), set(["a", "b", "c"]))
        flights = [f for f in sched.flights
                   if f.sobt == 420 or f.sobt == 600 or f.sobt == 800]
        self.assertEqual(set(sched.fleet["a"]), set(flights))
        self.assertEqual(sched.ending,
                         {"a": "LFML", "b": "LFML", "c": "LFBO"})

    def test_eta(self):
        sched = Simulator.load(
            _context.schedfile
        )
        flight = sched.flights[0]
        oracle_eta = flight.sobt + flight.duration
        self.assertEqual(Simulator.eta(sched, flight), oracle_eta)

        # Adding delay
        delay = 45
        sched.delays[flight] = delay  # in place
        oracle_eta_del = oracle_eta + delay
        self.assertEqual(Simulator.eta(sched, flight), oracle_eta_del)

    def test_next_focus(self):
        sched = Simulator.load(
            _context.schedfile
        )
        nflight = Simulator._next_focus(sched.flown, sched.delays, sched.fleet)
        self.assertEqual(Simulator.eta(sched, nflight), 440 + 110)
        self.assertEqual(nflight.destination, "LFPO")
        self.assertEqual(sched.assignment[nflight], "b")

        # Add delay to flight
        should_be_next = Simulator.Flight(origin="LFML", destination="LFPO",
                                          sobt=440, duration=110)
        sched.delays[should_be_next] = 800
        nnflight = Simulator._next_focus(sched.flown, sched.delays,
                                         sched.fleet)
        self.assertEqual(Simulator.eta(sched, nnflight), 660)
        self.assertEqual(sched.assignment[nnflight], "c")

    def test_following_flight(self):
        sched = Simulator.load(
            _context.schedfile
        )
        flight = Simulator.following_flight(sched)
        folflight = Simulator.Flight(
            origin="LFPO", destination="LFBO",
            sobt=600, duration=90
        )
        self.assertEqual(flight, folflight)

    def test_step_idle(self):
        """Test idle action"""
        sched = Simulator.load(_context.schedfile)
        idle = sched.assignment[sched.landed_flight]
        newsched, _, _, _ = Simulator.step(sched, idle)
        self.assertEqual(newsched.flights, sched.flights)
        self.assertEqual(newsched.fleet, sched.fleet)

    def test_swappable(self):
        sched = Simulator.load(_context.schedfile)
        self.assertTrue(Simulator.swappable(sched, "b"))
        self.assertFalse(Simulator.swappable(sched, "c"))

        # Test with an aircraft with no more flights
        schedv = Simulator.load(_context.schedvoidfile)
        newschedv, _, _, _ = Simulator.step(schedv, "a")
        self.assertTrue(Simulator.swappable(newschedv, "a"))

    def test_swap_void(self):
        """Tries to swap from flights to void."""
        sched = Simulator.load(_context.schedvoidfile)
        # Run first step without swapping,
        newsched, _, _, _ = Simulator.step(sched, "a")
        # a has finished, b has just arrived to LFPO
        # Now swap b and a:
        newsched, _, _, _ = Simulator.step(newsched, "a")
        self.assertEqual(len(newsched.fleet["b"]), 1)
        self.assertEqual(len(newsched.fleet["a"]), 2)

    def test_swap_fromvoid(self):
        """Swap from void, i.e. landed aircraft which should have finished
        take remaining flights of another aircraft."""
        sched = Simulator.load(_context.schedvoidfile)
        newsched, _, _, _ = Simulator.step(sched, "b")
        self.assertEqual(len(newsched.fleet["b"]), 1)
        self.assertEqual(len(newsched.fleet["a"]), 2)

    def test_terminal(self):
        sched = Simulator.load(_context.schedfile)
        action = sched.assignment[sched.landed_flight]
        done = False
        while not done:
            try:
                action = random.choice(list(sched.fleet.keys()))
                sched, _, done, _ = Simulator.step(sched, action)
            except AssertionError:
                continue
        self.assertIn(sched.landed_flight,
                      [Simulator.Flight(sobt=800, origin="LFBO",
                                        destination="LFML", duration=45),
                       Simulator.Flight(sobt=850, origin="LFLL",
                                        destination="LFML", duration=130),
                       Simulator.Flight(sobt=560, origin="LFLL",
                                        destination="LFBO", duration=100)])

    def test_action_swap(self):
        sched = Simulator.load(_context.schedfile)
        fl = Simulator.following_flight(sched)
        swapped_fl = Simulator.Flight(sobt=660, origin="LFPO",
                                      destination="LFLL", duration=80)
        swap = "b"
        newsched, _, _, _ = Simulator.step(sched, swap)
        self.assertEqual(newsched.assignment[swapped_fl], "a")
        self.assertEqual(newsched.assignment[fl], "b")
        self.assertNotEqual(newsched.fleet, sched.fleet)
        self.assertEqual(newsched.flights, sched.flights)

        # Check flights swaps
        self.assertEqual(
            newsched.fleet["b"][1:],
            [Simulator.Flight(sobt=600, origin="LFPO", destination="LFBO",
                              duration=90),
             Simulator.Flight(sobt=800, origin="LFBO", destination="LFML",
                              duration=45)])

    def test_d_ready_sobt(self):
        sched = Simulator.load(_context.schedfile)
        sched, _, _, _ = Simulator.step(sched, "a")
        drd = Simulator.d_ready_sobt(sched, "b")
        self.assertLess(drd, 0)
        drd = Simulator.d_ready_sobt(sched, "c")
        self.assertIsNone(drd)

    def test_flying_toward(self):
        sched = Simulator.load(_context.schedfile)
        self.assertTrue(Simulator.flying_toward(sched, "a", "LFBO"))
        self.assertTrue(Simulator.flying_toward(sched, "b", "LFPO"))
        self.assertTrue(Simulator.flying_toward(sched, "c", "LFBO"))

    def test_remaining(self):
        sched = Simulator.load(_context.schedfile)
        self.assertEqual(Simulator.remaining_flights(sched, "a"), 2)
        self.assertEqual(Simulator.remaining_flights(sched, "b"), 2)
        self.assertEqual(Simulator.remaining_flights(sched, "c"), 0)
