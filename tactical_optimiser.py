#!/usr/bin/python3
# -*- coding: utf-8 -*-
import argparse

import disman.agent as Agent
import disman.env as Env
import disman.info as Info


def main(args):
    simparam = {"p_severe_delay": args.severe_prob}
    initstate = Env.load(args.schedfile, simparam)
    episodes = []

    # Idle, do not do anything
    if args.idle:
        for i in range(args.episodes):
            initstate = Env.reset_delays(initstate, args.delays)
            rew, info = Agent.idle(initstate)
            episodes.append(info)
            print("Episode {}/{}".format(i, args.episodes), end="\r")

    # Random, do anything
    if args.random:
        for i in range(args.episodes):
            initstate = Env.reset_delays(initstate, args.delays)
            rew, info = Agent.random(initstate)
            episodes.append(info)
            print("Episode {}/{}".format(i, args.episodes), end="\r")

    # Rollout, use a trained agent
    elif args.rollout:
        with open(args.qlpath, "rb") as qf:
            agent = Agent.load(initstate, qf)

        for i in range(args.episodes):
            initstate = Env.reset_delays(initstate, args.delays)
            rew, info = Agent.rollout(agent, initstate)
            episodes.append(info)
            print("Episode {}/{}".format(i, args.episodes), ends="\r")

    # Training an agent
    else:
        vcs = ({}, 0)
        if args.qlpath is None:
            agent = Agent.make(initstate, args.qinit)
        else:
            with open(args.qlpath, "rb") as qf:
                agent = Agent.load(initstate, qf)

        if args.dqn_metric:
            state_pool = Agent.pick_states(initstate)
            state_pool_av = []  # List[float]

        eps = args.expvexp
        for i in range(args.episodes):
            print("Episode {}/{}".format(i, args.episodes), end="\r")
            initstate = Env.reset_delays(initstate, args.delays)
            agent, ar, vcs, d = Agent.train(agent, initstate,
                                            args.discount, eps, vcs[0],
                                            vcs[1])
            episodes.append(d)
            if args.dqn_metric:
                state_pool_av.append(Agent.eval_states(agent, state_pool))
        if args.qspath is not None:
            with open(args.qspath, "wb") as qf:
                Agent.save(agent, qf)
    if args.save_res is not None:
        processed = Info.process(episodes)
        Info.save(args.save_res, processed)
        if args.dqn_metric:
            with open(args.save_res + "_dqn.csv", "w") as f:
                for ast in state_pool_av:
                    f.write("{}\n".format(ast))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Q learning applied to disruption recovery problem")
    parser.add_argument(
        "--schedule",
        type=str,
        dest="schedfile",
        metavar="SCD",
        required=True,
        help="File containing schedule")
    parser.add_argument(
        "--idle",
        action="store_true",
        dest="idle",
        help="Use an idle agent")
    parser.add_argument(
        "--rollout",
        action="store_true",
        dest="rollout",
        help="Use the trained agent")
    parser.add_argument(
        "--random",
        action="store_true",
        dest="random",
        help="Use an agent choosing randomly actions")
    parser.add_argument(
        "--expvexp",
        type=float,
        dest="expvexp",
        metavar="ε",
        default=0.2,
        help="Parameter controlling exploration-exploitation tradeoff")
    parser.add_argument(
        "--discount",
        type=float,
        dest="discount",
        metavar="γ",
        default=1,
        help="Visibility of the agent")
    parser.add_argument(
        "--iterate",
        type=int,
        dest="episodes",
        metavar="N",
        default=1,
        help="Number of episoded to perform")
    parser.add_argument(
        "--load_q",
        type=str,
        dest="qlpath",
        metavar="path",
        help="Path to serialised Q matrix")
    parser.add_argument(
        "--save_q",
        type=str,
        dest="qspath",
        metavar="path",
        help="Destination file of Q matrix")
    parser.add_argument(
        "--severe_delay_prob",
        type=float,
        dest="severe_prob",
        metavar="SDP",
        default=0.,
        help="Probability of having a severe delay")
    parser.add_argument(
        "--delays",
        type=int,
        dest="delays",
        metavar="D",
        default=0,
        help="Number of delayed aircraft per simulation")
    parser.add_argument(
        "--qinit",
        dest="qinit",
        type=float,
        default=0.,
        help="Init value in the Q matrix")
    parser.add_argument(
        "--save_res",
        type=str,
        dest="save_res",
        metavar="RES",
        help="Save results to pandas file")
    parser.add_argument(
        "--dqn_metric",
        action="store_true",
        dest="dqn_metric",
        help="Compute the metric used in Google's DQN article")
    gargs = parser.parse_args()
    main(gargs)
