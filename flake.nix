{
  inputs = {
    flake-utils.url = github:numtide/flake-utils;
  };
  outputs = {
    self,
    nixpkgs,
    flake-utils,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      formatter = pkgs.alejandra;
      devShells.default = let
        extraLibs = with pkgs.python310Packages; [
          virtualenv
          tox
          mypy
          setuptools
          flake8
          numpy
          scipy
          pandas
          fastparquet
          # For tests
          coverage
          pytest
          pytest-cov
        ];
        ourPython = pkgs.python310.buildEnv.override {
            inherit extraLibs;
        };
      in
        pkgs.mkShell {
          buildInputs = [ourPython];
          shellHook = ''
            export LD_LIBRARY_PATH="${pkgs.stdenv.cc.cc.lib}/lib:''${LD_LIBRARY_PATH:+:}"
          '';
        };
    });
}
