from typing import Set, TypeVar, NamedTuple
from scipy import sparse

T = NamedTuple("T", [("matrix", sparse.spmatrix),
                     ("default", float)])
State = TypeVar("State")
Action = TypeVar("Action")


def train(ag: T, s: State, action_set: Set[Action], eps: float) -> Action: ...


def make(nstates: int, nactions: int) -> T: ...
