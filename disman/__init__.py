"""Init, loads files."""

import json
import os
from scipy import interpolate as spi
from numpy import random as spr
from pysistence import make_dict

DEFAULT_SIMPARAM = make_dict(p_severe_delay=0)
"""Default simulation parameters."""
MAX_DELAY = 300
"""Maximum delay considered"""

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
_DATADIR = os.path.join(ROOT_DIR, "../data/")
with open(os.path.join(_DATADIR, "atfm_static.json"), "r") as f:
    _ATFM_STATIC = json.load(f)

_RAW_ATFM_DELAY = spi.interp1d([elt[0] for elt in _ATFM_STATIC['iedf']],
                               [elt[1] for elt in _ATFM_STATIC['iedf']])
_ATFM_LOW_X = min([elt[0] for elt in _ATFM_STATIC['iedf']])

with open(os.path.join(_DATADIR, "delay_cost.json"), "r") as f:
    _DELAY_COST_TABLE = json.load(f)
_DELAY_TABLE_COL = [5, 15, 30, 60, 90, 120, 180, 240, 300]
_RAW_DELAY_COSTS = {k: spi.interp1d(_DELAY_TABLE_COL, _DELAY_COST_TABLE[k])
                    for k in _DELAY_COST_TABLE}

with open(os.path.join(_DATADIR, "taxi_time.json"), "r") as f:
    _TAXI_SPEC = json.load(f)


def atfm_delay() -> int:
    """Delay due to air traffic flow management."""
    draw = spr.random()
    if draw <= _ATFM_STATIC['prob']:
        # Stop before having too high values...
        newdraw = min(spr.random(), 0.999)
        delay = 0 if newdraw < _ATFM_LOW_X else int(_RAW_ATFM_DELAY(newdraw))
    else:
        delay = 0
    return delay


def no_atfm_delay() -> int:
    """Delay when on ground not due to departure,
    uses an exponential distribution of lambda = 15
    """
    lbd = 15
    return min(int(spr.exponential(lbd)), 90)


def taxi_time(station: str, in_out: str = "out") -> int:
    """Returns a probable taxi time for a station.

    Args:
        station: quite explicit
        io: either 'out' if the aircraft leaves the runway or 'in'
    """
    if station not in _TAXI_SPEC.keys():
        station = "EDWV"  # Low times
    return int(spr.normal(loc=_TAXI_SPEC[station]["mean_" + in_out],
                          scale=_TAXI_SPEC[station]["std_" + in_out]))


def delay_cost(time: int, model: str) -> float:
    """Returns the cost of delay.  Can raise an exception if the
    delay is too high.

    Args:
        time: delay amount in minutes
        model: model of aircraft
    """
    if time < _DELAY_TABLE_COL[0]:
        delay = 0
    else:
        delay = _RAW_DELAY_COSTS[model](min(time, MAX_DELAY))
    return delay
