"""Models the air traffic, the delays, the swaps."""
import copy
import pysistence as ps
import operator
import random
from functools import reduce
from typing import Optional, Dict, List, NamedTuple, Set, Tuple, TypeVar

import pandas as pd

from disman import (DEFAULT_SIMPARAM, MAX_DELAY, atfm_delay, delay_cost,
                    no_atfm_delay, taxi_time)

_A = TypeVar("_A")


INFOKEYS = set(["time", "dep_orig", "dep_dest",
                "misc", "taxi", "atfm", "reac_del",
                "dep_del", "dep_sobt", "dep_duration",
                "sev_added", "sev_dels",
                "tail_num", "cost", "swap", "swapwith"])
"""Keys in dict returned as informations."""
Info = Dict
"""Type of information."""

Flight = NamedTuple("Flight", [("origin", str), ("destination", str),
                               ("sobt", int), ("duration", int)])
"""Constant flight table"""

Action = str
"""Aircraft with which to swap."""

_GroundDelays = NamedTuple("_GroundDelays", [("atfm", int), ("no_atfm", int),
                                             ("taxi_out", int),
                                             ("taxi_in", int)])
Schedule = NamedTuple(
    "Schedule",
    [("landed_flight", Flight), ("ground_delays", _GroundDelays),
     ("delays", Dict[Flight, int]), ("assignment", Dict[Flight, str]),
     ("flown", Set[Flight]),
     ("flights", List[Flight]), ("stations", Set[str]),
     ("ending", Dict[str, str]), ("beginning", Dict[str, str]),
     ("fleet", Dict[str, List[Flight]]), ("fleet_models", Dict[str, str]),
     ("simparam", ps.persistent_dict.PDict)])
Schedule.__doc__ = """Data of the simulation

Args:
    landed_flight: the last landed flight
    ground_delays: delays encountered on the ground
    flights: immutable list of flights (immutable semantically, i.e.
             any new schedule should have a different flights list)
    stations: set of stations
    fleet: mapping from aircraft to their list of flights
    fleet_models: mapping from aircraft to their model, used to compute costs
    ending: where each aircraft should end the day
    beginning: where each aircraft begins the day
"""

_TURNOVER = 20
"""Time for a turnover."""
SEVERE_DELAY = 180
"""A delay considered severe, from 3h, airline must compensate."""


def _wrong_station_cost(model: str) -> float:
    """Cost if an aircraft does not end in the initially planned
    airport."""
    return delay_cost(MAX_DELAY, model)


def _severe_delay(prob: float) -> int:
    """Returns a heavy delay with proba [prob]."""
    return SEVERE_DELAY if random.random() <= prob else 0


def _take(xs: List[_A], x: _A):
    """Returns the head of [xs] up to [x] included."""
    ind_of_elt = xs.index(x)
    return xs[:ind_of_elt + 1]


def _drop(xs: List[_A], x: _A):
    """Drop the head of [xs] including [x] thus returning the tail"""
    ind = xs.index(x)
    return xs[ind + 1:]


def _landed_time(s: Schedule) -> int:
    """Returns the landing time of the focused flight"""
    return eta(s, s.landed_flight)


def _terminal(flights: List[Flight], flown: Set[Flight]) -> bool:
    """Returns whether the simulation is finished."""
    return set(flights) - flown == set()


def _next_focus(flown: Set[Flight], delays: Dict[Flight, int],
                fleet_: Dict[str, List[Flight]]) -> Flight:
    """Returns next landing flight.

    Args:
        flown: set containing all flown flights
        delays: mapping from flight to its current delay
        fleet: mapping from aircraft to its flight path
    """
    current_opflights = [_currently_in(flown, fleet_[a])
                         for a in fleet_]
    current_flights = [
        f for f in current_opflights if f is not None
    ]  # type: List[Flight]
    return min(current_flights, key=lambda f: f.sobt + f.duration + delays[f])


def _final_expected(fleet_: Dict[str, List[Flight]],
                    default: Dict[str, str] = None) -> Dict[str, str]:
    """Returns the final position expected for each aircraft."""

    def aux(acft: str) -> str:
        """Returns ending station of acft.  Uses default dict if flight list
        is empty."""
        if fleet_[acft] == []:
            stn = default[acft]  # type: ignore
        else:
            stn = fleet_[acft][-1].destination
        return stn
    return {k: aux(k) for k in fleet_}


def _currently_in(flown: Set[Flight],
                  flight_path: List[Flight]) -> Optional[Flight]:
    """Gives the flight the aircraft is performing.

    Args:
        flown: set of flown flights
        aircraft: tail number of aircraft to inspect
        flight_path: list of flights performed by the aircraft, sorted by sobt
        delays: maps flights to their delay

    Returns: the flight currently flown by the aircraft; if the aircraft is
    on the ground, it returns the departing flight; None if the aircraft
    has finished its day.
    """
    def loop(remfl: List[Flight]):
        if remfl == []:
            return None
        head = remfl[0]
        return (head if head not in flown
                else loop(remfl[1:]))
    return loop(flight_path)


def _prep_flights_fleet(raw_sched) -> Tuple[List[Flight],
                                            Dict[str, List[Flight]]]:
    """Returns flights and fleet dict."""
    flights = []  # type: List[Flight]
    fleet_ = {k: [] for k in
              raw_sched.tail_number}  # type: Dict[str, List[Flight]]

    def _aux(accflight: List[Flight], accfleet: Dict[str, List[Flight]],
             row) -> Tuple[List[Flight], Dict[str, List[Flight]]]:
        newflight = Flight(sobt=row.scheduled_departure,
                           origin=row.origin_airport,
                           destination=row.destination_airport,
                           duration=row.actual_fp_time)
        return ([newflight] + accflight,
                {**accfleet,
                 **{row.tail_number: [newflight] + accfleet[row.tail_number]}})

    flights, fleet_ = reduce(
        lambda acc, row: _aux(acc[0], acc[1], row),
        raw_sched.itertuples(), (flights, fleet_))

    fleet_ = {k: sorted(fleet_[k], key=lambda f: f.sobt) for k in fleet_}
    return (sorted(flights, key=lambda f: f.sobt), fleet_)


def _exchange_flights(landed: str, inbound: str,
                      fleet_: Dict[str, List[Flight]],
                      landed_next: Optional[Flight],
                      inbound_next: Optional[Flight],
                      assignment: Dict[Flight, str]
                      ) -> Tuple[Dict[str, List[Flight]],
                                 Dict[Flight, str]]:
    """Exchange tails of flights between two aircraft and return a new
    flight assignment accordingly.

    Args:
        landed: the landed aircraft tail number
        inbound: the inbound aircraft tail number
        fleet: mapping from aircraft to its flights
        landed_next: next flight of landed aircraft, to be given to
                     the inbound
        inbound_next: next flight of the inbound aircraft, to be given to
                      the landed
        assignment: mapping from flight to tail number

    Returns: new fleet and new assignment
    """
    landed_all = fleet_[landed]
    inbound_all = fleet_[inbound]

    landed_remaining = ([landed_next] + _drop(landed_all, landed_next)
                        if landed_next is not None
                        else [])
    landed_previous = (_take(landed_all, landed_next)[:-1]
                       if landed_next is not None
                       else landed_all)
    inbound_remaining = ([] if inbound_next is None
                         else [inbound_next] + _drop(inbound_all,
                                                     inbound_next))
    inbound_previous = (inbound_all if inbound_next is None
                        else _take(inbound_all, inbound_next)[:-1])

    assignment_landed_upd = {f: inbound for f in landed_remaining}
    assignment_inboud_upd = {f: landed for f in inbound_remaining}

    new_landed_all = landed_previous + inbound_remaining
    new_inbound_all = inbound_previous + landed_remaining

    newfleet = {**fleet_, **{landed: new_landed_all,
                             inbound: new_inbound_all}}

    # Ensure there is no 'lost' flight
    assert (len(new_landed_all + new_inbound_all) ==
            len(fleet_[landed] + fleet_[inbound]))

    assert new_landed_all == newfleet[landed], \
        "{}/{}".format(landed, inbound)
    assert inbound_next in new_landed_all + [None]
    assert inbound_next in newfleet[landed] + [None]  # type: ignore
    # assert landed_next in new_inbound_all
    # assert landed_next in newfleet[inbound]

    new_assignment = {**assignment, **assignment_inboud_upd,
                      **assignment_landed_upd}
    return newfleet, new_assignment


def _count_sevdel(sdl: Schedule) -> int:
    """Returns the number of severe delays currently."""
    delays = [current_delay(sdl, a) for a in sdl.fleet]
    return sum(d >= SEVERE_DELAY for d in delays)


def aircraft(s: Schedule, f: Flight = None) -> str:
    """Get aircraft performing flight.  If flight is not given,
    uses the landed flight."""
    return (s.assignment[f] if f is not None
            else s.assignment[s.landed_flight])


def fleet(sdl: Schedule) -> Set[str]:
    """Returns the aircraft in the fleet."""
    return set(sdl.fleet_models.keys())


def swappable(sdl: Schedule, aircraft_: str) -> bool:
    """[swappable s a] returns whether the landed aircraft can be
    swapped with aircraft [a].

    An aircraft `a` can be swapped with another one `b` if

    * the flight path of `b` lands at a time at the station where
      `a` is;
    * the sobt of the first flight of `b` departing from the station
      is not more than `MAX_DELAY` later than the current time;
    * the flight to be swapped in `b`'s flight path must not be the next
      one flown by `b` if `b` is on the ground, this avoids multiple
      swappings while `b` is waiting.
    """
    stn = sdl.landed_flight.destination
    fpth = sdl.fleet[aircraft_]  # type: List[Flight]
    if fpth == []:
        # Beginning as the flight path contains at least already flown flights,
        # being empty means the aircraft hasn't flown any flight
        return sdl.beginning[aircraft_] == stn
    curr = _currently_in(sdl.flown, sdl.fleet[aircraft_])
    if curr is None:  # None if aircraft has nothing to do
        rem_fpth = []  # type: List[Flight]
    else:
        rem_fpth = _drop(fpth, curr)
    if rem_fpth == []:
        # If the aircraft has finished its day, check if it rests
        # at the adequate station
        return fpth[-1].destination == stn
    # First check that the aircraft passes by the airport
    ini_following = following_flight(sdl)
    if ini_following is None:
        # If the landed flight has no next flight (finished flight path)
        # no need to check delay introduced
        geotime_compat = [f.origin == stn for f in rem_fpth]
    else:
        # Asserts that swapping does not introduce a delay greater than
        # the max time, for all but first remaining
        hdless_geotime_compat = [(f.origin == stn and
                                  abs(f.sobt - ini_following.sobt) <=
                                  SEVERE_DELAY) for f in rem_fpth[1:]]
        # If the first remaining flight has already been initialised,
        # do not count it as swappable, being initialised and the first
        # remaining flight means the aircraft is on the ground, waiting
        # to depart.  Its flight will thus not be swapped.
        head = rem_fpth[0]
        hd_geocomp = (sdl.delays[head] == 0 and
                      head.origin == stn and
                      abs(head.sobt - ini_following.sobt) <= MAX_DELAY)
        geotime_compat = [hd_geocomp] + hdless_geotime_compat
    return reduce(operator.or_, geotime_compat, False)


def _next_flight_by(sdl: Schedule, aircraft_: str,
                    station: str = None) -> Optional[Flight]:
    """[next_flight_by s a p] returns next flight of aircraft [a] departing
    from station [p] according to schedule [s].  If [p] is not given, uses
    the focused station.  Returns None if no flight lands at the
    station."""
    assert swappable(sdl, aircraft_) if station is None else True
    fpth = sdl.fleet[aircraft_]
    if fpth == []:
        # If fpth is empty, but at the right station
        return None
    curr = _currently_in(sdl.flown, sdl.fleet[aircraft_])
    rem_fpth = [] if curr is None else fpth[fpth.index(curr):]
    stn = sdl.landed_flight.destination if station is None else station

    # Seek the flight
    candidates = [f for f in rem_fpth if f.origin == stn]
    if candidates == []:
        # Since the aircraft has been declared swappable...
        return None
    head = candidates[0]
    if head == rem_fpth[0]:
        return (head if sdl.delays[head] == 0
                else (candidates[1]
                      if len(candidates) >= 2
                      else None))
    return head


def d_ready_sobt(sdl: Schedule, aircraft: str) -> Optional[int]:
    """Return time difference between ready time of the landed
    aircraft and the sobt  of the swappable flight of the
    swappable aircraft."""
    if not swappable(sdl, aircraft):
        return None
    nf = _next_flight_by(sdl, aircraft, sdl.landed_flight.destination)
    if nf is None:
        return None
    rd = (_landed_time(sdl) + sdl.ground_delays.taxi_out + _TURNOVER +
          sdl.ground_delays.taxi_out)
    return rd - nf.sobt


def flying_toward(sdl: Schedule, aircraft: str, station: str = None) -> bool:
    """Return whether the aircraft is currently flying toward the
    station.  If station is None, uses the current station"""
    curr = _currently_in(sdl.flown, sdl.fleet[aircraft])
    if curr is None:
        return False
    return curr.destination == (station if station is not None else
                                sdl.landed_flight.destination)


def set_delays(sdl: Schedule, delays: int) -> Schedule:
    """Returns a new schedule with new delayed flights.

    Args:
        sdl: schedule used as basis
        delays: number of flights to delay
    Returns: a schedule with delayed flights.
    """
    del_flights = set(random.choice(sdl.flights)
                      for _ in range(delays))
    nsimparam = ps.make_dict({**sdl.simparam.without("delays"),
                              **{"delays": del_flights}})
    return Schedule(
        landed_flight=sdl.landed_flight,
        stations=sdl.stations,
        ground_delays=sdl.ground_delays,
        delays=sdl.delays,
        assignment=sdl.assignment,
        flown=sdl.flown,
        flights=sdl.flights,
        ending=sdl.ending,
        beginning=sdl.beginning,
        fleet=sdl.fleet,
        fleet_models=sdl.fleet_models,
        simparam=nsimparam)


def load(schedfile: str, simparam: Dict = DEFAULT_SIMPARAM) -> Schedule:
    """Loads data from a schedule file.

    Args:
        schedfile: csv file containing as columns ac_type, tail_number,
                   origin_airport, destination_airport, scheduled_departure,
                   actual_fp_time
        simparam: misc parameters for the simulation
    """
    raw_schedule = pd.read_csv(schedfile)
    raw_schedule = raw_schedule[raw_schedule.tail_number.notnull()]

    stations = set([])  # type: Set[str]
    stations = (set(raw_schedule.origin_airport) |
                set(raw_schedule.destination_airport))

    flights = []  # type: List[Flight]
    fleet_ = {}  # type: Dict[str, List[Flight]]
    flights, fleet_ = _prep_flights_fleet(raw_schedule)

    # Ending stations
    beginning = {k: fleet_[k][0].origin for k in fleet_}
    ending = {}  # type: Dict[str, str]
    ending = _final_expected(fleet_)

    init_assignment = {}  # type: Dict[Flight, str]
    init_assignment = reduce(
        lambda acc, acft: reduce(
            lambda af_flgts, flgt: {**af_flgts,
                                    **{flgt: acft}},
            fleet_[acft], acc),
        fleet_, {})
    init_delays = {}  # type: Dict[Flight, int]
    init_delays = {k: 0 for k in flights}

    # fleet models
    fleet_models = {}  # type: Dict[str, str]
    fleet_models = {rowt.tail_number: rowt.ac_type.lower()
                    for rowt in raw_schedule.itertuples()}

    # Just if we want to process something
    nsimparam = ps.make_dict({**simparam, **{"delays": set()}})

    # Initialisation
    first_flight = min(flights, key=lambda f: f.sobt + f.duration)
    fst_delays = _GroundDelays(
        atfm=atfm_delay(), no_atfm=no_atfm_delay(),
        taxi_out=taxi_time(first_flight.destination, "out"),
        taxi_in=taxi_time(first_flight.destination, "in"))

    return Schedule(
        landed_flight=first_flight,
        ground_delays=fst_delays,
        flights=flights,
        stations=stations,
        assignment=init_assignment,
        delays=init_delays,
        flown=set([first_flight]),
        fleet=fleet_,
        ending=ending,
        beginning=beginning,
        fleet_models=fleet_models,
        simparam=nsimparam)


def eta(s: Schedule, f: Flight) -> int:
    """Returns estimated time of arrival of flight, taking into account
    previously encountered delays."""
    prevdel = s.delays[f]
    return f.sobt + f.duration + prevdel


def current_delay(s: Schedule, a: str = "") -> int:
    """[current_delay s a] returns the current delay of aircraft [a] according
    to schedule [s]."""
    if a == "":
        curr_flown = s.landed_flight  # type: Optional[Flight]
    elif s.fleet[a] == []:  # Can happen sometimes...
        curr_flown = None
    else:
        curr_flown = _currently_in(s.flown, s.fleet[a])
    return 0 if curr_flown is None else s.delays[curr_flown]


def remaining_flights(sdl: Schedule, acft: str = "") -> int:
    """Returns the number of flights to be performed by an aircraft.

    Args:
        sdl: the Schedule to use
        acft: aircraft whose number of flights is to be returned,
              if not specified, the focused aircraft is used
    Returns:
        the number of *complete* flights remaining to perform, i.e.
        the currently flown one is not considered
    """
    if acft == "" or acft == sdl.assignment[sdl.landed_flight]:
        jaf = sdl.landed_flight
        jaacft = sdl.assignment[jaf]
        remfl = [f for f in sdl.fleet[jaacft] if f.sobt > jaf.sobt]
    else:
        curr_fl = _currently_in(sdl.flown, sdl.fleet[acft])
        if curr_fl is None:
            remfl = []
        else:
            remfl = [f for f in sdl.fleet[acft] if f.sobt > curr_fl.sobt]
    return len(remfl)


def following_flight(s: Schedule) -> Optional[Flight]:
    """Returns the next flight flown by the aircraft having landed; or the
    void flight if there is no next flight.  Is based on the order of the
    flights in schedule.fleet
    """
    landed_acft = s.assignment[s.landed_flight]

    assert s.landed_flight in s.fleet[landed_acft], \
        "lf:{}\nla:{}".format(s.landed_flight, landed_acft)

    ind_of_landedfl = s.fleet[landed_acft].index(s.landed_flight)

    if ind_of_landedfl >= len(s.fleet[landed_acft]) - 1:
        # If no following flight
        nflight = None
    else:
        nflight = s.fleet[landed_acft][ind_of_landedfl + 1]
    return nflight


def step(s: Schedule, a: Action) -> Tuple[Schedule, float, bool, Info]:
    """Apply the action on the schedule,

    [step s a] applies action [a] on schedule [s] and returns the new
    schedule and an associated cost
    """
    cost = 0.
    landed_aircraft = s.assignment[s.landed_flight]

    new_flown = s.flown | set([s.landed_flight])
    swap = False

    # If action is superior to the number of flights, nothing is done
    if a != landed_aircraft:
        swap = True
        l_nextf = following_flight(s)  # landed next flight
        # * ii stands for initially inbound, flight given to the focused
        #   aircraft
        ii_nextf = _next_flight_by(s, a)
        ii_acft = a

        assert (ii_nextf is None or ii_nextf in s.fleet[ii_acft]), \
            ("Assignment/fleet incoherence:\naircraft:{}\n"
             "flight:{}\n"
             "fleet:{}".format(ii_acft, ii_nextf, s.fleet[ii_acft]))

        assert (ii_nextf is None or
                ii_nextf.origin == s.landed_flight.destination), \
            "Wrong origin\nnext:{},\ndep:{}".format(ii_nextf, l_nextf)

        nfleet, nassignment = _exchange_flights(
            landed_aircraft, ii_acft, s.fleet, l_nextf, ii_nextf,
            s.assignment)

        departing_flight = ii_nextf  # type: Optional[Flight]
    else:
        nfleet = copy.deepcopy(s.fleet)
        nassignment = copy.deepcopy(s.assignment)
        departing_flight = following_flight(s)

    assert (departing_flight is None or
            departing_flight.origin == s.landed_flight.destination), \
        "{} {}".format(s.landed_flight, departing_flight)

    # compute delays
    if departing_flight is not None:
        can_depart_at = (_landed_time(s) + s.ground_delays.taxi_in +
                         _TURNOVER + s.ground_delays.no_atfm
                         + s.ground_delays.taxi_out)
        depart_at = max(departing_flight.sobt, can_depart_at)
        cobt = depart_at + s.ground_delays.atfm
        reactionary = max(0, can_depart_at - departing_flight.sobt -
                          s.ground_delays.no_atfm)
        added_severe = min(SEVERE_DELAY,
                           (_severe_delay(s.simparam["p_severe_delay"]) +
                            (SEVERE_DELAY
                             if departing_flight in s.simparam["delays"]
                             else 0)))
        dep_del = min(MAX_DELAY, cobt + added_severe -
                      departing_flight.sobt)  # type: Optional[int]
        if dep_del is None:
            raise ValueError
        ndelays = {**s.delays, **{departing_flight: dep_del}}
        cost += delay_cost(dep_del, s.fleet_models[landed_aircraft])
    else:
        # Set vars to zero for info struct
        dep_del, added_severe, cost = None, 0, 0.
        ndelays = copy.deepcopy(s.delays)

    done = _terminal(s.flights, new_flown)
    if done:
        # # Compute extra costs if aircraft do not end in the right airport
        # aircraft_pos = _final_expected(s.fleet, default=s.beginning)
        # wpcost = reduce(
        #     lambda acc, acft: (acc +
        #                        _wrong_station_cost(s.fleet_models[acft])
        #                        if aircraft_pos[acft] != s.ending[acft]
        #                        else acc),
        #     aircraft_pos, 0.)
        # cost += wpcost

        newschedule = Schedule(
            landed_flight=s.landed_flight,
            ground_delays=s.ground_delays,
            flights=s.flights,
            delays=s.delays,
            assignment=nassignment,
            stations=s.stations,
            flown=new_flown,
            fleet=nfleet,
            ending=s.ending,
            beginning=s.beginning,
            fleet_models=s.fleet_models,
            simparam=s.simparam)
    else:
        # Prepare next step
        next_landing_flight = _next_focus(new_flown, ndelays, nfleet)
        next_destination = next_landing_flight.destination
        next_delays = _GroundDelays(
            atfm=atfm_delay(), no_atfm=no_atfm_delay(),
            taxi_out=taxi_time(next_destination, "out"),
            taxi_in=taxi_time(next_destination, "in"))

        newschedule = Schedule(
            landed_flight=next_landing_flight,
            ground_delays=next_delays,
            flights=s.flights,
            delays=ndelays,
            assignment=nassignment,
            stations=s.stations,
            flown=new_flown,
            fleet=nfleet,
            ending=s.ending,
            beginning=s.beginning,
            fleet_models=s.fleet_models,
            simparam=s.simparam)

    info = {"time": _landed_time(s),
            "dep_orig": s.landed_flight.destination,
            "dep_dest": (departing_flight.destination
                         if departing_flight is not None
                         else None),
            "atfm": s.ground_delays.atfm,
            "misc": s.ground_delays.no_atfm,
            "taxi": s.ground_delays.taxi_in + s.ground_delays.taxi_out,
            "reac_del": (None if departing_flight is None
                         else reactionary),
            "dep_del": dep_del,
            "dep_sobt": (departing_flight.sobt if departing_flight is not None
                         else None),
            "dep_duration": (departing_flight.duration
                             if departing_flight is not None
                             else None),
            "sev_added": added_severe > 0,
            "sev_dels": _count_sevdel(newschedule),
            "cost": cost,
            "tail_num": landed_aircraft,
            "swap": swap,
            "swapwith": a}
    return (newschedule, cost, done, info)
